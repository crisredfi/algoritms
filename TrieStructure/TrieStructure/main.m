//
//  main.m
//  TrieStructure
//
//  Created by Crisredfi on 08/09/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WordsGenerator.h"





int main(int argc, const char * argv[])
{

    @autoreleasepool {
        WordsGenerator *words = [WordsGenerator new];
        [words initializeWordGenerator];
           }
    return 0;
}

