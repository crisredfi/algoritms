//
//  AppDelegate.h
//  NSArrayCategories
//
//  Created by crisredfi on 24/09/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

