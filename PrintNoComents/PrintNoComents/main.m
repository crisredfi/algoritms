//
//  main.m
//  PrintNoComents
//
//  Created by Crisredfi on 16/07/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        // insert code here...
        
        
        
        NSString *test = @"this is a test and this line // shouldnt be printed \n we shoild continue here and show nothing from here /* this is not printed \n nor here \n */ to here";
        
        // enumerate string by line.
        __block BOOL comentsEnabled = NO;
        __block NSMutableString *myString = [NSMutableString stringWithFormat:@""];

        [test enumerateLinesUsingBlock:^(NSString *line, BOOL *stop) {
            [line enumerateSubstringsInRange:NSMakeRange(0, line.length) options:NSStringEnumerationByComposedCharacterSequences
                                  usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                if (!comentsEnabled) {
                    if ([substring isEqualToString:@"/"]) {
                        if ([line characterAtIndex:substringRange.location + 1] == '/') {
                            // ignore the rest.
                            *stop = YES;
                        } else if ([line characterAtIndex:substringRange.location + 1] == '*') {
                            comentsEnabled = YES;
                        } else {
                            [myString appendString:substring];
                        }
                    } else {
                        
                        [myString appendString:substring];
                    }
                }
                if ([substring isEqualToString:@"*"] &&
                           [line characterAtIndex:substringRange.location + 1] == '/') {
                    
                    comentsEnabled = NO;
                    
                }
                                      
            }];
            [myString appendString:@"\n"];
            }];
        NSLog(@"%@", myString);

        
        
        
        
    }
    return 0;
}

