//
//  MappingAlphabet.m
//  subsets
//
//  Created by Crisredfi on 13/08/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import "MappingAlphabet.h"

@interface MappingAlphabet()

@property (nonatomic, copy) NSString *mainString;
@end

@implementation MappingAlphabet

- (NSArray *)getValidCombinationForString:(NSString *)input {
    _mainString = input;

    NSInteger ret = [self getSusetsWithIndex:1 andNumber:[[_mainString substringWithRange:NSMakeRange(0, 1)]integerValue]];
    
    NSLog(@"characters %ld", (long)ret);
    return nil;
}


- (NSInteger)getSusetsWithIndex:(NSInteger)indx andNumber:(NSInteger)value {
    
    if (value > 26) {
       return 0;
    }
    if (value == 0) {
        return 0;
    }
    NSLog(@"value %ld", (long)value);
    if (indx == _mainString.length)  {
        return 1;
    }
    
    NSInteger ret = [self getSusetsWithIndex:indx + 1 andNumber: value * 10 +
                     [[_mainString substringWithRange:NSMakeRange(indx, 1)] integerValue]];
    
    ret += [self getSusetsWithIndex:indx + 1
                          andNumber:[[_mainString substringWithRange:NSMakeRange(indx, 1)] integerValue]];
    return ret;
}


@end

