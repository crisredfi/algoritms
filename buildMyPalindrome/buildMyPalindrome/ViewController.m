//
//  ViewController.m
//  buildMyPalindrome
//
//  Created by aleix on 14/08/2014.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import "ViewController.h"
#import "buildMyPalindrom.h"
#import <objc/objc.h>

@interface ViewController ()

@end

@implementation ViewController
            
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    buildMyPalindrom *palindromw = [[buildMyPalindrom alloc] init];
    NSLog(@"palindrome %@", [palindromw buildPalindromeFromString:@"aabbcceee"]);
    NSLog(@"palindrome %@", [palindromw buildPalindromeFromString:@""]);

     id LenderClass = objc_getClass("AppDelegates");
    NSLog(@"log");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
