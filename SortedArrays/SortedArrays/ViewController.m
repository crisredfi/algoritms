//
//  ViewController.m
//  SortedArrays
//
//  Created by crisredfi on 23/09/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end


NSInteger inSort(id num1, id num2, void *context) {
    int v1 = [num1 intValue];
    int v2 = [num2 intValue];
    
    if (v1 > v2) {
        return NSOrderedDescending;
    } else if (v1 < v2) {
        return NSOrderedAscending;
    }
    return NSOrderedSame;
}


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSArray* input = @[
  @[@2, @5, @10],
  @[@25, @100, @105],
  @[@7, @56, @42],
  ];
    
    NSMutableArray *sortedArray = [NSMutableArray arrayWithArray:input[0]];
    NSData *hint = [sortedArray sortedArrayHint];
    
    for (int i = 1; i < [input count]; i++) {
        [sortedArray addObjectsFromArray:input[i]];
        NSArray *temporalSort = [sortedArray sortedArrayUsingFunction:inSort context:nil hint:hint];
        sortedArray = [temporalSort mutableCopy];
        hint = [sortedArray sortedArrayHint];
    }
    NSLog(@"sorted is %@", sortedArray);
    
    NSMutableSet *mutableInput = [NSMutableSet set];
    
    [input enumerateObjectsUsingBlock:^(NSArray *obj, NSUInteger idx, BOOL *stop) {
        [mutableInput addObjectsFromArray:obj];
    }];
    NSArray *allObjects = [mutableInput allObjects];
    NSSortDescriptor *sortOrder = [NSSortDescriptor sortDescriptorWithKey:nil ascending:YES];
    NSLog(@"another sort %@", [allObjects sortedArrayUsingDescriptors:@[sortOrder]]);
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
