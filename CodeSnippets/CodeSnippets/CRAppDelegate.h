//
//  CRAppDelegate.h
//  CodeSnippets
//
//  Created by Crisredfi on 17/08/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CRAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
