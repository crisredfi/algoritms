//
//  ViewController.m
//  twotableviews
//
//  Created by crisredfi on 20/10/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import "ViewController.h"
#import "TableVCOne.h"
#import "TableVCTwo.h"

@interface ViewController ()

@property (nonatomic) TableVCOne *tableOne;
@property (nonatomic) TableVCTwo *tableTwo;

@end

@implementation ViewController

- (void)awakeFromNib {
    [super awakeFromNib];
    self.tableOne = [[TableVCOne alloc] init];
    self.tableTwo = [[TableVCTwo alloc] init];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (tableView.tag == 0) {
        return [self.tableOne tableView:tableView cellForRowAtIndexPath:indexPath];
    } else {
            return [self.tableTwo tableView:tableView cellForRowAtIndexPath:indexPath];
    }
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag == 0) {
        return [self.tableOne tableView:tableView numberOfRowsInSection:section];
    } else {
        return [self.tableTwo tableView:tableView numberOfRowsInSection:section];
    }
}

@end
