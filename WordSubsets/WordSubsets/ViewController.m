//
//  ViewController.m
//  WordSubsets
//
//  Created by crisredfi on 17/09/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import "ViewController.h"

typedef void (^blockSubsetIterate)(NSSet *words);

@interface SubsetsClass :NSObject

-(void)createSusetWithSet:(NSSet*)words andCharacter:(NSString *)character withCompletititonBlock:(blockSubsetIterate)block;
@end

@implementation SubsetsClass



-(void)createSusetWithSet:(NSSet*)words andCharacter:(NSString *)character withCompletititonBlock:(blockSubsetIterate)block {
    
    NSMutableSet *newSubset = [NSMutableSet set];
    [words enumerateObjectsUsingBlock:^(NSString *subset, BOOL *stop) {
        [newSubset addObject:[subset stringByAppendingString:character]];
        
    }];
    [newSubset unionSet:words];
    block(newSubset);
}

@end

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    SubsetsClass *subsets = [[SubsetsClass alloc] init];
    
    NSSet *set = [NSSet setWithObjects:@"a", @"b", @"c", @"d", nil];
    __block NSMutableSet *mutableSet = [NSMutableSet setWithObject:@""];
    for (NSString *substring in [[set allObjects] sortedArrayUsingSelector:@selector(compare:)] ) {
        [subsets createSusetWithSet:[mutableSet copy] andCharacter:substring withCompletititonBlock:^(NSSet *words) {
            
            mutableSet = [words mutableCopy];
        }];
    }
    NSLog(@"mutable set is %@", [[mutableSet allObjects] sortedArrayUsingSelector:@selector(compare:)]);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
