//
//  main.m
//  binarySearch
//
//  Created by Aleix Segon on 30/07/2014.
//  Copyright (c) 2014 mastercard. All rights reserved.
//

#import <Foundation/Foundation.h>

NSInteger intSort(id num1, id num2, void *context)
{
    int v1 = [num1 intValue];
    int v2 = [num2 intValue];
    if (v1 < v2)
        return NSOrderedAscending;
    else if (v1 > v2)
        return NSOrderedDescending;
    else
        return NSOrderedSame;
}


int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        // insert code here...
        NSMutableArray *mutableArray = [[NSMutableArray alloc] init];

        for (NSInteger i = 0; i < 1000; i++) {
            [mutableArray addObject:@(i)];
        }
        
        NSData *hint = [mutableArray sortedArrayHint];
        //now some trik in here..
        for (NSInteger i = 1000; i < 1050; i++) {
            [mutableArray insertObject:@(i) atIndex:arc4random_uniform((u_int32_t)i) ];
        }
        
        NSArray *sorted = [mutableArray sortedArrayUsingFunction:intSort context:NULL hint:hint];
        NSLog(@"mutable array %@", sorted);
        for(NSInteger i = [mutableArray count] -1; i > 0; i--) {
            NSInteger dest = arc4random_uniform((u_int32_t)i);
            [mutableArray exchangeObjectAtIndex:i withObjectAtIndex:dest];
        }
        NSLog(@"is random? %@", mutableArray);
    }
    
    return 0;
}

