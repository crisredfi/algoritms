//
//  main.m
//  Allcombinations
//
//  Created by crisredfi on 30/10/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^recurseBlock) (NSArray * words);

@interface recrusion : NSObject
+ (void)recurseWithContainer:(NSArray *)input withWords:(NSArray *)output withBlock:(recurseBlock)block;
@end

@implementation recrusion

+ (void)recurseWithContainer:(NSArray *)input withWords:(NSArray *)output withBlock:(recurseBlock)block {
    NSMutableArray *temporalWords = [input mutableCopy];
    
    [input enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        NSMutableArray *temporalOutput = [output mutableCopy];
        [temporalWords removeObject:obj];
        if ([output count] > 0 ) {
            [output enumerateObjectsUsingBlock:^(NSString *obj2, NSUInteger idx2, BOOL *stop2) {
                NSString *newWord = [obj2 stringByAppendingString:obj];
                [temporalOutput addObject:newWord];
            }];
            
        } else {
            [temporalOutput addObject:obj];
        }
       [recrusion recurseWithContainer:[temporalWords copy]
                             withWords:[temporalOutput copy] withBlock:block];
        block(temporalOutput);

    }];
    
}

@end


int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
        NSMutableSet *array = [NSMutableSet set];
        [recrusion recurseWithContainer:@[@"w", @"x", @"y", @"z"] withWords:@[] withBlock:^(NSArray *words) {
            [array addObjectsFromArray:words];
            
        }];
        
        NSLog(@"words %@", [[array allObjects] sortedArrayUsingSelector:@selector(compare:)]);
        
    }
    return 0;
}
