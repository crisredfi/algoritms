//
//  treeNode.m
//  transverseTree
//
//  Created by Aleix Segon on 22/07/2014.
//  Copyright (c) 2014 mastercard. All rights reserved.
//

#import "treeNode.h"

typedef void (^treeBlock)(treeNode *tree,
                          BOOL *end);

@implementation treeNode


- (void) insert:(treeNode *)node
{
    if(node.data > self.data) // using properties, could also use fields ->
    {
        if(self.right != nil) {
            [self.right insert:node];
        }  else {
            self.right = node;
            self.right.rootNode = self;
        }
    }
    else if(node.data < self.data)
    {
        if(self.left != nil) {
            [self.left insert:node];
        } else {
            self.left = node;
            self.left.rootNode = self;
        }

    }
}


//[self transverseTreeWithBlock:^(NSMutableArray *tree, BOOL **end) {
//    
//}];
- (void)transverseTreeWithBlock:(treeBlock)block {
    [self.left transverseTreeWithBlock:block];
    if (block) {
        BOOL stop = NO;
        block(self, &stop);
        if (stop) {
            return;
        }
    }
    [self.right transverseTreeWithBlock:block];

}


- (void)traverseTree {
    
    NSMutableArray *values = [NSMutableArray array];
    [self transverseTreeWithBlock:^(treeNode *tree,
                                    BOOL *end) {
        
        [values addObject:@(tree.data)];
    }];
    
    if ([values count] % 2 != 0) {
     
        NSLog(@"median value is %@", [values objectAtIndex:values.count/2]);
    } else {
        
        NSInteger sum = [[values objectAtIndex:values.count/2] integerValue] +
        [[values objectAtIndex:(values.count/2) + 1] integerValue] ;
        NSLog(@"median value is %f", (double)sum / 2);

    }
}

@end
