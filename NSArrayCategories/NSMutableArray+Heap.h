//
//  NSMutableArray+Heap.h
//  NSArrayCategories
//
//  Created by crisredfi on 24/09/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Heap)

- (void)balanceArrayWithAscendingOrder:(BOOL)isAscending;

@end
