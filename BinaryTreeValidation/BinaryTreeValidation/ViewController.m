//
//  ViewController.m
//  BinaryTreeValidation
//
//  Created by crisredfi on 23/09/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import "ViewController.h"
#import "treeNode.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    treeNode *TreeRoot;
    
    for (NSInteger i = 9; i > 0; i-- ){
        treeNode *node = [treeNode new];
        node.data = i;
        if(TreeRoot != nil)
            [TreeRoot insert:node];
        else {
            TreeRoot = node;
        }
    }
    for (NSInteger i = 9; i < 19; i++ ){
        treeNode *node = [treeNode new];
        node.data = i;
        if(TreeRoot != nil)
            [TreeRoot insert:node];
    }
    
    
    [TreeRoot validateTree];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
