//
//  ViewController.m
//  keychain
//
//  Created by Aleix Segon on 21/07/2014.
//  Copyright (c) 2014 mastercard. All rights reserved.
//

#import "ViewController.h"
@import security;


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSLog (@"%@", [[UIDevice currentDevice] name]);
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
