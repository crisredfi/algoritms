//
//  main.m
//  CodeSnippets
//
//  Created by Crisredfi on 17/08/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CRAppDelegate.h"
bool isAnagram(NSArray *input) {
    
    NSMutableSet *anagrams = [NSMutableSet setWithCapacity:input.count];
    
    [input enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        NSMutableSet *characters = [NSMutableSet set];
        [obj enumerateSubstringsInRange:NSMakeRange(0, obj.length)
                                options:NSStringEnumerationByComposedCharacterSequences
                             usingBlock:^(NSString *substring,
                                          NSRange substringRange,
                                          NSRange enclosingRange,
                                          BOOL *stop) {
                                 // we now have an array of characters
                                 [characters addObject:substring];
                             }];
        if ([anagrams containsObject:characters]) {
            *stop = YES;
        } else {
            [anagrams addObject: characters];
        }
    }];
    
    return ([anagrams count] == [input count]) ? NO:YES;
}

int main(int argc, const char * argv[])
{
    
    @autoreleasepool {
        
        NSArray *input = @[@"bag", @"bat", @"tac"];
        
        if (isAnagram(input)) {
            NSLog(@"is anagram");
        }
    }
    return 0;
}
//
//int main(int argc, char * argv[])
//{
//    @autoreleasepool {
//        
//        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CRAppDelegate class]));
//    }
//}
