//
//  main.m
//  anagrams
//
//  Created by Crisredfi on 08/07/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

bool isAnagram(NSArray *input) {
    
    NSMutableSet *anagrams = [NSMutableSet setWithCapacity:input.count];
    NSMutableSet *characters = [NSMutableSet set];
    
    [input enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        [obj enumerateSubstringsInRange:NSMakeRange(0, obj.length)
                                options:NSStringEnumerationByComposedCharacterSequences
                             usingBlock:^(NSString *substring,
                                          NSRange substringRange,
                                          NSRange enclosingRange,
                                          BOOL *stop) {
                                 // we now have an array of characters
                                 [characters addObject:substring];
                             }];
        if ([anagrams containsObject:characters]) {
            *stop = YES;
        } else {
            [anagrams addObject: characters];

        }
    }];

    return ([anagrams count] == [input count]) ? NO:YES;
}

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        NSArray *input = @[@"bag", @"bat", @"tab"];
        
        if (isAnagram(input)) {
            NSLog(@"is anagram");
        }
    }
    return 0;
}

