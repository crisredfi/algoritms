//
//  FindCombinations.h
//  alphabetCombinations
//
//  Created by crisredfi on 15/09/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FindCombinations : NSObject

+ (NSInteger)getSoultionsForStringInput:(NSString *)input;

@end
