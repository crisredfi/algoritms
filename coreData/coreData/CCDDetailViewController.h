//
//  CCDDetailViewController.h
//  coreData
//
//  Created by aleix on 13/08/2014.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCDDetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
