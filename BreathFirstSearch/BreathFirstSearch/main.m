//
//  main.m
//  BreathFirstSearch
//
//  Created by Crisredfi on 17/08/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>
const NSInteger NUM_LINKED_NODES = 2;


@interface Node : NSObject
{
	NSSet *linkedNodes;
}
@property (nonatomic, retain) NSSet *linkedNodes;
@end

@implementation Node
@synthesize linkedNodes;
@end


NSArray *addDecreasingLinksToNodes(NSArray *nodes)
{
	NSMutableArray *allLinks = [NSMutableArray arrayWithCapacity:[nodes count] / NUM_LINKED_NODES];
	
	NSInteger i;
	for (i = 0; i < [nodes count]; i += NUM_LINKED_NODES)
	{
		Node *link = [[Node alloc] init];
		[allLinks addObject:link];
        
		NSSet *links = [NSSet setWithObject:link];
		
		NSInteger j;
		for (j = i; j < i + NUM_LINKED_NODES; j++)
		{
			((Node *)[nodes objectAtIndex:j]).linkedNodes = links;
		}
	}
	
	return allLinks;
}

NSArray *addIncreasingLinksToNodes(NSArray *nodes)
{
	NSMutableArray *allLinks = [NSMutableArray arrayWithCapacity:[nodes count] * NUM_LINKED_NODES];
	
	for (Node *node in nodes)
	{
		NSMutableSet *links = [NSMutableSet setWithCapacity:NUM_LINKED_NODES];
		NSInteger i;
		for (i = 0; i < NUM_LINKED_NODES; i++)
		{
			Node *link = [[Node alloc] init];
			[links addObject:link];
			[allLinks addObject:link];
		}
		
		node.linkedNodes = links;
	}
	
	return allLinks;
}



// breath first search goes trhough each node and check childs.
// check first its node and then iterates through its child to check for more visits

void breathFirstSearch(Node *startingNode) {
    
    NSMutableArray *queue = [NSMutableArray array];
    NSMutableSet *visitedNodes = [NSMutableSet set];
    
    [queue addObject:startingNode];
    
    while ([queue count] > 0) {
        @autoreleasepool {
            Node *visitedNode = [queue firstObject];
            [queue removeObject:visitedNode];
            if (![visitedNodes containsObject:visitedNode]) {
                [visitedNodes addObject:visitedNode];
                [queue addObjectsFromArray:[visitedNode.linkedNodes allObjects]];
            }
        }
    }
    NSLog(@"visited nodes %lu", (unsigned long)[visitedNodes count]);
}


NSMutableSet *visitedDeepNodes;

void deepFirstSearch(Node *searchNode) {
    if (!visitedDeepNodes) {
        // first time visiting the recursive function. lazy load the container
        // and add the first node to it
        visitedDeepNodes = [NSMutableSet set];
        [visitedDeepNodes addObject:searchNode];
        
    }
    
    for (Node *newNode in searchNode.linkedNodes) {
            if (![visitedDeepNodes containsObject:newNode]) {
                [visitedDeepNodes addObject:newNode];
                deepFirstSearch(newNode);
            }
    }
    
}



int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        // insert code here...
        const NSInteger HALF_GRAPH_DEPTH = 20;
        Node *head = [[Node alloc] init];
        NSArray *nodeArray = [NSArray arrayWithObject:head];
        
        NSInteger i;
        NSInteger totalNodeCount = 1;
        for (i = 0; i < HALF_GRAPH_DEPTH; i++)
        {
            nodeArray = addIncreasingLinksToNodes(nodeArray);
            totalNodeCount += [nodeArray count];
        }
        for (i = 0; i < HALF_GRAPH_DEPTH; i++)
        {
            nodeArray = addDecreasingLinksToNodes(nodeArray);
            totalNodeCount += [nodeArray count];
        }
        NSLog(@"Created %ld nodes", totalNodeCount);
        
       // breathFirstSearch(head);
        deepFirstSearch(head);
        NSLog(@"visited deep nodes count %lu", (unsigned long)visitedDeepNodes.count);
    }
    return 0;
}

