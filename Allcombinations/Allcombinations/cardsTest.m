//
//  cardsTest.m
//  Allcombinations
//
//  Created by crisredfi on 01/11/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import "cardsTest.h"

typedef NS_ENUM(NSUInteger, cardtyp) {
    hear,
    sword,
    club
};

@interface cardsTest()

@property (assign, nonatomic) cardtyp hear;

@end

@implementation cardsTest

@end
