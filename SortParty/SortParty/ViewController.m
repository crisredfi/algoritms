//
//  ViewController.m
//  SortParty
//
//  Created by crisredfi on 16/09/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import "ViewController.h"

@interface User : NSObject


@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *userSurname;
@property (nonatomic) NSNumber *age;

@end

@implementation User

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ %@, %lu", self.userName, self.userSurname, [self.age longValue]];
}


@end


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self createSortParty];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

NSInteger intSort(id num1, id num2, void *context)
{
    int v1 = [num1 intValue];
    int v2 = [num2 intValue];
    if (v1 < v2)
        return NSOrderedAscending;
    else if (v1 > v2)
        return NSOrderedDescending;
    else
        return NSOrderedSame;
}


- (void)createSortParty {
    
    NSArray *names = @[@"Aleix", @"Andrea", @"Alber", @"Paul", @"Carlos", @"Anna", @"Anna", @"Marta", @"Vero"];
    NSArray *surnames = @[@"Guri", @"Aparicio", @"Raich", @"Bosch", @"Cancio", @"Santos", @"Carlota", @"Franqueses", @"Bautista"];
    NSArray *age = @[@32, @23, @28, @33, @35, @50, @42, @19, @30];
    
    NSMutableArray *elements = [NSMutableArray array];
    [names enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        User *newUser = [[User alloc] init];
        newUser.userName = names[idx];
        newUser.userSurname = surnames[idx];
        newUser.age = age[idx];
        [elements addObject:newUser];
    }];
    
    NSLog(@"unsorted list %@", elements);
    
    NSSortDescriptor *byName = [NSSortDescriptor sortDescriptorWithKey:@"userName" ascending:YES];
    NSArray *byNameSort = [elements sortedArrayUsingDescriptors:@[byName]];
    NSLog(@"byName list %@", byNameSort);
    
    NSSortDescriptor *bySurname = [NSSortDescriptor sortDescriptorWithKey:@"userSurname" ascending:YES];
    NSArray *bySurnameSort = [elements sortedArrayUsingDescriptors:@[bySurname]];
    NSLog(@"bysurname list %@", bySurnameSort);

    bySurnameSort = [elements sortedArrayUsingDescriptors:@[byName, bySurname]];
    NSLog(@"name and Surname %@", bySurnameSort);

    NSArray *customSort = [elements sortedArrayUsingComparator:^NSComparisonResult(User *obj1, User *obj2) {
        if (obj1.age > obj2.age) {
            return NSOrderedDescending;
        } else if (obj1.age < obj2.age) {
            return NSOrderedAscending;
        }
        return NSOrderedSame;
    }];
    
    NSLog(@"by age %@", customSort);
    NSData *hint = [customSort sortedArrayHint];
    
    NSArray *resortedArray = [customSort sortedArrayUsingFunction:intSort context:nil hint:hint];
    
    // bynary add
    NSUInteger index = [customSort indexOfObject:elements[0]
                                       inSortedRange:NSMakeRange(0, customSort.count)
                                         options:NSBinarySearchingFirstEqual
                                 usingComparator:^NSComparisonResult(User *obj1,  User *obj2) {
                                   
                                     if (obj1.age > obj2.age) {
                                         return NSOrderedDescending;
                                     } else if (obj1.age < obj2.age) {
                                         return NSOrderedAscending;
                                     }
                                     return NSOrderedSame;
        
    }];
    
    NSLog(@"index is %li", index);

    
}

@end
