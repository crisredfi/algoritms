//
//  HeapStructure.h
//  heapDataStructure
//
//  Created by Crisredfi on 21/07/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HeapStructure : NSObject

- (void)addNode:(NSNumber*)newNode;
- (void)removeFirstObject;

@end
