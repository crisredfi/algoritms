//
//  main.m
//  pairs
//
//  Created by Crisredfi on 04/07/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>
#define kNumbers 10000000



void calculatepairs(unsigned int index, NSMutableArray *input) {
    if (index > 1) {
        unsigned int exponential = pow(index, 2);

        for (unsigned int i = 0; exponential < kNumbers; i++) {
            NSMutableDictionary *dictionary = input[exponential];
            if ([dictionary[@"number"] intValue] % index == 0) {
                dictionary[@"isAccepted"] = @NO;
            }
            exponential = pow(index, 2) + index*i;
        }
    }
    
}

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
      __block  NSMutableArray *numbers = [NSMutableArray arrayWithCapacity:kNumbers];
        
        for (unsigned int i = 0; i < kNumbers; i++) {
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:i], @"number" ,
                                         [NSNumber numberWithBool:YES], @"isAccepted", nil];
            [numbers addObject:dict];
        }
    
        [numbers enumerateObjectsUsingBlock:^(NSMutableDictionary *numb,
                                              NSUInteger indx,
                                              BOOL*finish) {
            if ([numb[@"isAccepted"] boolValue]) {
                calculatepairs((unsigned int)indx, numbers);
            }
            
        }];

        [numbers enumerateObjectsUsingBlock:^(NSMutableDictionary *numb,
                                              NSUInteger indx,
                                              BOOL*finish) {
            if ([numb[@"isAccepted"] boolValue]) {
                NSLog(@"%i", [numb[@"number"] intValue]);

            }
            
        }];

        
        // insert code here...
        NSLog(@"Hello, World!");
        
    }
    return 0;
}

