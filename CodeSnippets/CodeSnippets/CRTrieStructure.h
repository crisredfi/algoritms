//
//  CRTrieStructure.h
//  CodeSnippets
//
//  Created by Crisredfi on 08/09/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CRTrieStructure : NSObject

@property (nonatomic, copy) NSString *value;
@property (nonatomic) NSMutableArray *childs;

@end
