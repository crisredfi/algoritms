//
//  AppDelegate.h
//  subsets
//
//  Created by Crisredfi on 13/08/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

