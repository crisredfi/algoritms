//
//  treeNode.h
//  transverseTree
//
//  Created by Aleix Segon on 22/07/2014.
//  Copyright (c) 2014 mastercard. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface treeNode : NSObject

@property (nonatomic) NSInteger data;
@property (nonatomic) treeNode* left;
@property (nonatomic) treeNode* right;
@property (nonatomic) treeNode* rootNode;


- (void) insert:(treeNode *)node;
- (void)traverseTree;

@end
