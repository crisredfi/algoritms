//
//  FindCombinations.m
//  alphabetCombinations
//
//  Created by crisredfi on 15/09/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import "FindCombinations.h"

typedef void (^combinationsblock) ();

@implementation FindCombinations

+ (void)generateSolutionsForInput:(NSArray *)input
                 withCurrentIndex:(NSUInteger)indx
                previousAvailable:(BOOL)isAvailable
            withCompletitionBlock:(combinationsblock)block {
  
    if (indx > [input count] -1) {
        block();
        return;
    }
    NSString *current = input[indx];

    if (indx > 0 && isAvailable) {
        NSString *previous = input[indx -1];
        NSString *sumString = [previous stringByAppendingString:current];
        NSInteger sumInteger = [sumString integerValue];
        indx++;
        if (sumInteger <= 26) {
            // we have a sum, we will then need to check substring to sum them.
            [self generateSolutionsForInput:input withCurrentIndex:indx previousAvailable:NO withCompletitionBlock:block];
            // we cant now join this with the previous...
        }
        [self generateSolutionsForInput:input withCurrentIndex:indx previousAvailable:YES withCompletitionBlock:block];
    } else {
        indx++;
        [self generateSolutionsForInput:input withCurrentIndex:indx previousAvailable:YES withCompletitionBlock:block];

    }
    

}

+ (NSInteger)getSoultionsForStringInput:(NSString *)input {
    
    NSMutableArray *inputCharacters = [NSMutableArray array];
    [input enumerateSubstringsInRange:NSMakeRange(0, input.length)
                              options:NSStringEnumerationByComposedCharacterSequences
                           usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                                  [inputCharacters addObject:substring];
                              }];
    
    __block NSInteger counter = 0;
    
    [FindCombinations generateSolutionsForInput:inputCharacters
                               withCurrentIndex:0
                              previousAvailable:YES
                          withCompletitionBlock:^() {
                              counter++;
                              
                          }];
    
    NSLog(@"get solutions %li", (long)counter);
    return counter;
    
}





@end
