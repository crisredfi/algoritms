//
//  main.m
//  LongStringParty
//
//  Created by crisredfi on 26/09/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

typedef void (^numbersBlock)(NSString *word);

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
//
//
//void getSubstrings(NSDictionary *pattern,
//                               NSMutableArray *input,
//                               NSString *word,
//                   numbersBlock block) {
//    
//    if ([input count] == 0) {
//        block(word);
//    } else {
//        
//        NSString *inputKey = [input objectAtIndex:0];
//        NSMutableArray *copiedInput = [input mutableCopy];
//        [copiedInput removeObjectAtIndex:0];
//        NSNumber *number = [NSNumber numberWithInteger:[inputKey integerValue]];
//        NSArray *letters = pattern[number];
//        if (letters) {
//            for (int i = 0; i < [letters count]; i++) {
//                NSString *character = letters[i];
//                NSString *newWord = [word stringByAppendingString:character];
//                getSubstrings(pattern, copiedInput, newWord, block);
//            }
//        } else {
//            getSubstrings(pattern, copiedInput, word, block);
//        }
//    }
//}
//
//
//
//
//int main (int argc, const char * argv[])
//{
//    @autoreleasepool {
//        
//        NSDictionary* dict = @{@2: @[@"A", @"B", @"C"],
//                               @3: @[@"D", @"E", @"F"],
//                               @4: @[@"G", @"H", @"I"],
//                               @5: @[@"J", @"K", @"L"],
//                               @6: @[@"M", @"N", @"O"],
//                               @7: @[@"P", @"Q", @"R", @"S"],
//                               @8: @[@"T", @"U", @"V"],
//                               @9: @[@"W", @"X", @"Y", @"Z"]};
//        
//        NSString *inputPhone = @"25349";
//        NSMutableArray *characters = [NSMutableArray array];
//        for (int i = 0; i < inputPhone.length; i++) {
//            NSString *substring = [inputPhone
//                                   substringWithRange:NSMakeRange(i, 1)];
//            [characters addObject:substring] ;
//        }
//        NSMutableArray *listOfWords = [NSMutableArray array];
//        NSLog(@"list of words %@", listOfWords);
//      getSubstrings(dict, characters, @"", ^(NSString *word) {
//          NSLog(@"%@", word);
//      });
//    }
//}
//
