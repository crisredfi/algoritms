//
//  main.m
//  CrackingInterview1.1
//
//  Created by Crisredfi on 23/07/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        // insert code here...
        NSString *one = @"aabcccccaaa ";
        
        NSMutableArray *characters = [NSMutableArray arrayWithCapacity:one.length];
        [one enumerateSubstringsInRange:NSMakeRange(0, one.length)
                               options:NSStringEnumerationByComposedCharacterSequences
                            usingBlock:^(NSString *substring,
                                         NSRange substringRange,
                                         NSRange enclosingRange,
                                         BOOL *stop) {
                                
                                [characters addObject:substring];
                            }];
        
        int counter = 0;
        NSMutableString *output = [[NSMutableString alloc] init];
        
        for (NSString *character in characters) {
            if (counter == 0) // new number
            {
                [output appendString:character];
                counter++;
            } else  if ([character characterAtIndex:0] == [output characterAtIndex:output.length - 1]) {
                counter++;
            } else {
                [output appendString:[NSString stringWithFormat:@"%i", counter]];
                [output appendString:character];
                counter = 1;
            }
        }
        [output appendString:[NSString stringWithFormat:@"%i", counter]];

        NSLog(@"%@", output);
//
//        if ([characters count] == [one length]) {
//            NSLog(@"they are different!");
//        } else {
//            NSLog(@"they are not different");
//        }
//        NSArray *arrau = @[];
    }
    return 0;
}

