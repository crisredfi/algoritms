//
//  NSString+myLevienshtein.m
//  DistanceWord
//
//  Created by Crisredfi on 13/07/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import "NSString+myLevienshtein.h"

@implementation NSString (myLevienshtein)


- (NSInteger)compareString:(NSString *)comparableString {
    
    NSString *stringA = [[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] lowercaseString];
    NSString *stringB = [[comparableString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] lowercaseString];
    
    NSInteger x = [stringA length];
    NSInteger y = [stringB length];
    
    NSInteger i, j, value;
    NSInteger *d;

    if (x++ > 0 && y++ > 0) {
        d = malloc(sizeof(NSInteger) * y * x);
        for (NSInteger k = 0; k < x; k++) {
            d[k] = k;
        }
        for (NSInteger k = 0; k < y; k++) {
            d[k * x] = k;
        }
        
        for (i = 1; i < x; i++) {
            for(j = 1; j < y; j++) {
                if ([stringA characterAtIndex:i-1] == [stringB characterAtIndex:j-1]) {
                    value = 0;
                } else {
                    value = 1;

                }
                
                d[ j * x + i ] = MIN(d[ (j - 1) * x + i ] + 1, MIN(d[ j * x + i - 1 ] +  1, d[ (j - 1) * x + i -1 ] + value));

                
    
                
            }
        }
        
    }
    NSInteger distance = d[ x * y - 1 ];
    free(d);
    return distance;
    
}

@end
