//
//  main.m
//  swapWords
//
//  Created by Crisredfi on 16/07/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

NSString * swapString(NSString *inputString) {
    
    NSMutableArray *characters = [NSMutableArray array];
    [inputString enumerateSubstringsInRange:NSMakeRange(0, inputString.length)
                                    options:NSStringEnumerationByComposedCharacterSequences | NSStringEnumerationReverse
                                 usingBlock:^(NSString *substring,
                                              NSRange substringRange,
                                              NSRange enclosingRange,
                                              BOOL *stop) {
                                     [characters addObject:substring];
                                 }];
    return [characters componentsJoinedByString:@""];
}



int main(int argc, const char * argv[])
{

    @autoreleasepool {

//        NSString *input = @"the boy ran";
//    
//        NSArray *words = [input componentsSeparatedByString:@" "];
//        NSMutableString *outputString = [NSMutableString stringWithFormat:@""];
//        [words enumerateObjectsUsingBlock:^(NSString *word, NSUInteger idx, BOOL *stop) {
//            
//            [outputString appendString:swapString(word)];
//            [outputString appendString:@" "];
//        }];
//        // insert code here...
//        NSLog(@"%@", outputString);
        NSString *input = @"hello world";
        
        NSMutableArray *characters = [NSMutableArray array];
        // blocks not available here..
        for (NSUInteger i = 0; i+1 < input.length; i++) {
            [characters addObject:[input substringWithRange:NSMakeRange(i, 1)]];
            
        }
        NSMutableArray *array = [NSMutableArray arrayWithObjects:@2, @5, @9, @14, @16, @17, @18, @21, @22, nil];
        id newObject = @15;
        
        
//        NSUInteger newIndex = [array indexOfObject:newObject
//                                     inSortedRange:(NSRange){0, [array count]}
//                                           options:NSBinarySearchingInsertionIndex
//                                   usingComparator:comparator];
//        
//        [array insertObject:newObject atIndex:newIndex];
//        NSLog (@"array %@", array);

    }
    return 0;
}

