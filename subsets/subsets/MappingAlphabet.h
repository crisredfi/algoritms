//
//  MappingAlphabet.h
//  subsets
//
//  Created by Crisredfi on 13/08/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MappingAlphabet : NSObject
- (NSArray *)getValidCombinationForString:(NSString *)input;

@end
