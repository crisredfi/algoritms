//
//  main.m
//  transverseTree
//
//  Created by Aleix Segon on 22/07/2014.
//  Copyright (c) 2014 mastercard. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "treeNode.h"


int main(int argc, const char * argv[])
{

    @autoreleasepool {
        treeNode *TreeRoot;

        for (NSInteger i = 9; i > 0; i-- ){
            treeNode *node = [treeNode new];
            node.data = i;
            if(TreeRoot != nil)
                [TreeRoot insert:node];
            else {
                TreeRoot = node;
            }
        }
        for (NSInteger i = 9; i < 19; i++ ){
            treeNode *node = [treeNode new];
            node.data = i;
            if(TreeRoot != nil)
                [TreeRoot insert:node];
        }
        
        
        [TreeRoot traverseTree];

        
        // insert code here...
        NSLog(@"Hello, World!");
        
    }
    return 0;
}

