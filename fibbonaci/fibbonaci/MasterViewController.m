//
//  MasterViewController.m
//  fibbonaci
//
//  Created by Aleix Segon on 06/08/2014.
//  Copyright (c) 2014 mastercard. All rights reserved.
//

#import "MasterViewController.h"

#import "DetailViewController.h"
#define increase 25

@interface MasterViewController () {
    NSMutableArray *_objects;
    NSMutableDictionary *fibboNumbers;
    NSUInteger counter;
}
@end

@implementation MasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    counter = 0;
    [self generateDispatch];
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;

    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
    
    
}

- (NSUInteger)fibbonacci:(NSUInteger)first {
    
    NSString *key = [NSString stringWithFormat:@"%lu", (unsigned long)first];
    if (fibboNumbers[key] != nil) {
        return [fibboNumbers[key] integerValue];
    }
    if(first == 0) {
        [fibboNumbers setObject:[NSNumber numberWithUnsignedInteger:0] forKey:key];
        
        return 0;
    } else if(first == 1) {
        [fibboNumbers setObject:[NSNumber numberWithUnsignedInteger:1] forKey:key];
        return 1;
    } else {
        NSInteger number = [self fibbonacci:first - 1] + [self fibbonacci:first - 2];
        [fibboNumbers setObject:[NSNumber numberWithUnsignedInteger:number] forKey:key];
        return number;
    }
}

- (void)generateDispatch {
    // not handled the point that we reach to the end of fibo sequence marqued my max UIInteger.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self calculateFibonacci];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [self generateDispatch];
            
        });
    });
}


- (void)calculateFibonacci {

    // lazyt load
    counter += increase;
    if (fibboNumbers == nil) {
        fibboNumbers = [[NSMutableDictionary alloc]  init];
    }
    for (NSUInteger i = counter - increase; i < counter; i++) {
        NSUInteger inte = [self fibbonacci:i];
      
        if ((inte + [self fibbonacci:i+1]) >= NSUIntegerMax) {
            break;
        }
    }
    NSLog(@"fibo %@", fibboNumbers);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender
{
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
    [_objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return fibboNumbers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    //NSDate *object = _objects[indexPath.row];
    NSString *stirng = [NSString stringWithFormat:@"%lu", (unsigned long)indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)[fibboNumbers[stirng] integerValue]];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDate *object = _objects[indexPath.row];
        [[segue destinationViewController] setDetailItem:object];
    }
}

@end
