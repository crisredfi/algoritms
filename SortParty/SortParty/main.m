//
//  main.m
//  SortParty
//
//  Created by crisredfi on 16/09/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface BlahClass : NSObject

@property (nonatomic, copy) dispatch_block_t myBlock;

@end


@implementation BlahClass

- (instancetype)init {
    if (self = [super init]) {
        __weak BlahClass *weakSelf = self;
        //somewhere in the code
        _myBlock = ^{
            NSLog(@"%@", weakSelf);
        };
    }
    return self;
}

-(void)doSomethingWithBlock:(dispatch_block_t)block
{
    self.myBlock = block;
}

@end


int main(int argc, char * argv[]) {
    @autoreleasepool {
//        NSArray *input = [NSArray arrayWithObjects:@1, @2, @1, @3, @1, @4, @3, @1, @4, nil];
//        
//        NSCountedSet *countSet = [NSCountedSet setWithArray:input];
//        
//        NSUInteger repetitions = [countSet countForObject:@1];
//        
        // return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
