//
//  HeapStructure.m
//  heapDataStructure
//
//  Created by Crisredfi on 21/07/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import "HeapStructure.h"

@interface HeapStructure()
@property (nonatomic) NSMutableArray *heap;
@end

@implementation HeapStructure

- (instancetype)init
{
    self = [super init];
    if (self) {
    
        _heap = [NSMutableArray arrayWithCapacity:13];
    
    }
    return self;
}



- (void)removeFirstObject
{
    if ([_heap count] > 1) {
        [_heap removeObjectAtIndex:0];
        [self rebalanceArray];
    }
    
}


- (void)rebalanceArray
{
    for (NSUInteger i = _heap.count - 1; i >= 1;) {
        NSNumber *number = _heap[i];
        NSNumber *parentNumber = _heap[i/2];
        if ([parentNumber integerValue] < [number integerValue]) {
            // swap
            [_heap exchangeObjectAtIndex:i withObjectAtIndex:i/2];
        }
        // handle tree structure pair/impair leaves.
        if (i%2!=0) {
            i--;
        } else {
            i = i/2;
        }
    }
}


- (void)addNode:(NSNumber *)newNode {
    
    [_heap addObject:newNode];
    if ([_heap count] > 1) {
    // count is bigger than 1, we need to rebalance the tree.
        [self rebalanceArray];
    }
}

@end
