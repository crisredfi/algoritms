//
//  main.m
//  allpossibleWords
//
//  Created by Crisredfi on 04/07/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BlahClass : NSObject

@property (nonatomic, copy) dispatch_block_t myBlock;

@end


//@implementation BlahClass
//
////dispatch_block_t block = ^{
////    NSLog(@"%@", self);
////};
////__weak BlahClass *weakSelf = self;
////somewhere in the code
//- (void)test {
//    
//  
//    [self doSomethingWithBlock:block];
//}
//
//- (void)doSomethingWithBlock:(dispatch_block_t) block
//{
//    
//    self.myBlock = block;
//}
//
//@end

void getAllPossibleWords( NSString *word, NSMutableArray *letters, NSMutableSet *solution) {
    
    if ([letters count] > 0) {
        for (unsigned int i = 0; i< [letters count]; i++) {
            
            NSString *newWord = [word stringByAppendingString:letters[i]];
            NSMutableArray *newLetters = [letters mutableCopy];
            [newLetters removeObjectAtIndex:i];
            getAllPossibleWords(newWord,  newLetters, solution);
            //[letters sortedArrayUsingSelector:@selector(compare:)];
            
        }
    } else {
        [solution addObject:word];
    }
}


int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        NSString *word = @"albaricoq";
         NSMutableArray *leters = [NSMutableArray arrayWithCapacity:word.length];
        
        [word enumerateSubstringsInRange:NSMakeRange(0, [word length])
                                 options:(NSStringEnumerationByComposedCharacterSequences)
                              usingBlock:^(NSString *substring,
                                           NSRange substringRange,
                                           NSRange enclosingRange,
                                           BOOL *stop) {
                                  [leters addObject:substring];
                              }];
        
        // recursion
        
        NSMutableSet *solution = [NSMutableSet set];
        getAllPossibleWords(@"", leters, solution);
        // insert code here...
        NSLog(@"solution %@ number %lu", solution  , (unsigned long)[solution count]);
        
    }
    return 0;
}




