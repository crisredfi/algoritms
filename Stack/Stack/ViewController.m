//
//  ViewController.m
//  Stack
//
//  Created by crisredfi on 29/10/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import "ViewController.h"
#import "Stack.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    Stack *stack = [[Stack alloc] init];
    [stack push:3];
    [stack push:5];
    [stack push:23];
    [stack push:3];
    [stack push:5];
    [stack push:23];
    while (true) {
        NSUInteger value = [stack pop];
        if (value == 0) {
            return;
        } else {
            NSLog(@"value %li", value);
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
