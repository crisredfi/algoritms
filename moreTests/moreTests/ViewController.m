//
//  ViewController.m
//  moreTests
//
//  Created by crisredfi on 09/10/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic) NSDictionary *phoneCharacters;
@property (nonatomic) NSArray *phoneNumber;

@end


typedef void (^phoneNumberBlock) (NSString *word);
@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
//    NSArray *input = @[@1, @2,  @4];
//   NSArray *subset = @[@1, @2, @3, @5, @9, @12, @14, @17, @21];
//    
//    [self nonRepeatedSubsets:@[@"w", @"x", @"y", @"z"]];
//    NSInteger value = 1;
//    NSInteger index = [self getIndexForValue:value fromArray:subset withMinBounds:0 andMaxBounds:[subset count] -1];
//    NSLog(@"%li", index);
    
//    NSArray *phoneNu = @[@4, @9, @7, @1, @9, @2, @7];
//    [self getWordsCombinationsFromPhoneNumber:phoneNu];
//    [self binarySearch];
//    [self removeCharactersFromString];
    [self reverseWords];
}


- (void)getWordsCombinationsFromPhoneNumber:(NSArray *)phoneNum {
    _phoneNumber = phoneNum;
   _phoneCharacters = @{@2 : @[@"a", @"b", @"c"],
                              @3 : @[@"d", @"e", @"f"],
                              @4 : @[@"g", @"h", @"i"],
                              @5 : @[@"j", @"k", @"l"],
                              @6 : @[@"m", @"n", @"o"],
                              @7 : @[@"p", @"q", @"r", @"s"],
                              @8 : @[@"t", @"u", @"v"],
                              @9 : @[@"w", @"x", @"y", @"z"]};
    
    [self getPhoneNumeberCombinationWithCurrentString:@"" withIndex:0 withCompletitionBlock:^(NSString *word) {
        NSLog(@"%@", word);
    }];
}

- (void)removeCharactersFromString {
    
    NSString *myString = @"helloWorld";
    myString = [myString stringByReplacingOccurrencesOfString:@"l"
                                        withString:@""];
    NSLog(@"mystring %@", myString);
    
}


- (void)getPhoneNumeberCombinationWithCurrentString:(NSString *)word withIndex:(NSInteger)idx withCompletitionBlock:(phoneNumberBlock)block {
    
    if (idx > [_phoneNumber count]-1) {
        block(word);
        return;
    }
    
    NSInteger number = [_phoneNumber[idx] integerValue];
    if (number < 2) {
        NSString *newWord = [word stringByAppendingString:[NSString stringWithFormat:@"%li", number]];
        [self getPhoneNumeberCombinationWithCurrentString:newWord withIndex:idx+1 withCompletitionBlock:block];
    }
    
    NSArray *characters = _phoneCharacters[@(number)];
    [characters enumerateObjectsUsingBlock:^(NSString *character, NSUInteger idex, BOOL *stop) {
        NSString *newWord = [word stringByAppendingString:character];
        [self getPhoneNumeberCombinationWithCurrentString:newWord withIndex:idx+1 withCompletitionBlock:block];
    }];
}


- (void)reverseWords {
    NSString *word  = @"Do or do not, there is no try.";
    NSMutableString *output = [[NSMutableString alloc] init];
    NSArray *substring = [word componentsSeparatedByString:@" "];
    
//    [word enumerateSubstringsInRange:NSMakeRange(0,word.length) options:NSStringEnumerationByWords | NSStringEnumerationReverse usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
//        [output appendString:substring];
//        if (enclosingRange.location + enclosingRange.length == word.length) {
//            [output appendString:@"."];
//        }
//        [output appendString:@" "];
//    }];
    for (NSInteger i = (substring.count -1); i >=0; i--) {
        [output appendString:substring[i]];
          [output appendString:@" "];
    }
    NSLog(@"out %@", output);
    
    
}



- (void)binarySearch {
    NSArray *sortedArray =  @[@1, @2, @3, @5, @6, @7, @8];
    NSInteger intege = [sortedArray indexOfObject:@3 inSortedRange:NSMakeRange(0, sortedArray.count) options:NSBinarySearchingFirstEqual usingComparator:^NSComparisonResult(id obj1, id obj2) {
        if (obj1 < obj2) {
            return  NSOrderedAscending;
        }
        if (obj1 > obj2) {
            return NSOrderedDescending;
        }
        return NSOrderedSame;
    }];
    NSLog(@"search inserted %li", intege);

}

















typedef void (^combinationBlock)(NSMutableSet *mySet);

- (void)combineFromIndex:(NSInteger)index withArray:(NSArray *)input withWord:(NSString *)word block:(combinationBlock)block {

    NSString *character = input[index];
    NSMutableSet *temporalSet = [NSMutableSet set];
    NSMutableString *newWord = [NSMutableString stringWithString:word];
    [newWord appendString:character];
    [temporalSet addObject:[newWord copy]];
    
    for (NSInteger i = index +1; i < [input count]; i++) {
        [self combineFromIndex:i withArray:input withWord:newWord block:block];
    }
    block(temporalSet);


}


- (void)nonRepeatedSubsets:(NSArray *)input {
    NSMutableSet *allCombinations = [NSMutableSet set];
    
    [input enumerateObjectsUsingBlock:^(NSString *character, NSUInteger idx, BOOL *stop) {
  
        [self combineFromIndex:idx withArray:input
                      withWord:[[NSMutableString alloc] init]
                         block:^(NSMutableSet *mySet) {
                             [allCombinations unionSet:mySet];
        }];
        

    }];
    
    NSLog(@"mySet %@ num %li", allCombinations, [allCombinations count]);

}




- (void)allsubsetsOfstring:(NSString *)input {
    NSMutableSet *substrings = [NSMutableSet set];
    NSMutableArray *characters = [NSMutableArray array];
    [input enumerateSubstringsInRange:NSMakeRange(0, input.length)
                              options:NSStringEnumerationByComposedCharacterSequences
                           usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
        [characters addObject:substring];
    }];
    
    for (int i = 0; i < [characters count]; i++) {
        if ([substrings count] == 0) {
            [substrings addObject:characters[i]];
        } else {
        
        NSMutableSet *temporalSet = [substrings mutableCopy];
        [temporalSet enumerateObjectsUsingBlock:^(NSString *obj, BOOL *stop) {
            for (int j = 0; j <= obj.length; j++) {
                NSMutableString *temporalString = [NSMutableString stringWithString:obj];
                [temporalString insertString:characters[i] atIndex:j];
                [substrings addObject:temporalString];
            }
            [substrings removeObject:obj];
        }];
        }
    }
    
    NSLog(@"substring %@ count %lu", substrings, (unsigned long)[substrings count]);
    
    
    
    
}




- (NSInteger)getIndexForValue:(NSInteger )value
                      fromArray:(NSArray *)array
                  withMinBounds:(NSInteger)minBounds
                   andMaxBounds:(NSInteger)maxBounds {
    
    if ([array count] <= maxBounds || [array count] == 0|| array == nil) {
        return -1;
    }
    
    NSInteger mid = (maxBounds + minBounds)/2;
    NSInteger tempValue = [array[mid] integerValue];
    
    
    if (tempValue < value) {
    
        return [self getIndexForValue:value fromArray:array withMinBounds:mid+1 andMaxBounds:maxBounds];
    } else if (tempValue > value) {
        return  [self getIndexForValue:value fromArray:array withMinBounds:minBounds andMaxBounds:mid];
    } else if (value == tempValue) {
        
        return mid;
    }
    return -1;

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
//
//- (BOOL)inputArray:(NSArray *)input hasSubset:(NSArray *)subset {
//    NSMutableSet *inputSet = [NSMutableSet setWithArray:input];
//    NSMutableSet *subsetSet = [NSMutableSet setWithArray:subset];
//    
//    
//    [inputSet minusSet:subsetSet];
//    NSLog(@"input set treatened %@", inputSet);
//    NSLog(@"input subset %@", subsetSet);
//    return [subsetSet isSubsetOfSet:inputSet];
//    
//}
//
//
//
//- (double)squareRoot:(long)number {
//    
//    if (number <= 0) return 0;
//    
//    long half = number/2;
//    double squareRoot = number/half;
//    
//    double decimal = 0;
//    if ((number%half) != 0) {
//        decimal = number%half;
//        return squareRoot + (decimal * 0.1);
//    }
//    
//    return (double)squareRoot;
//    
//    
//}

@end
