//
//  ViewController.m
//  NSArrayCategories
//
//  Created by crisredfi on 24/09/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import "ViewController.h"
#import "NSMutableArray+Heap.h"

@interface ViewController ()

@property (nonatomic) NSMutableArray *maxHeap;
@property (nonatomic) NSMutableArray *minHeap;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    //NSMutableArray *ar = [NSMutableArray arrayWithObjects:@10, @9, @8, @7, nil];
    //[ar balanceArrayWithAscendingOrder:NO];
    //NSLog(@"%@", ar);
    _maxHeap = [NSMutableArray array];
    _minHeap = [NSMutableArray array];
    
    NSArray *insertSequence = @[@2, @7, @4, @9, @1, @5, @8, @3, @6, @10];
    [insertSequence enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [self addObjectAndBalanceHeaps:obj];
    }];
}



- (void)addObjectAndBalanceHeaps:(id)object {
    //NSUInteger maxHeaphead = [[_maxHeap firstObject] unsignedIntegerValue];
    NSUInteger minHeapHead = [[_minHeap firstObject] unsignedIntegerValue];
    if ([object unsignedIntegerValue] >= minHeapHead) {
        [_maxHeap addObject:object];
        [_maxHeap balanceArrayWithAscendingOrder:NO];
    } else {
        [_minHeap addObject:object];
        [_minHeap balanceArrayWithAscendingOrder:YES];
    }
    [self balanceArrays];
}

- (void)balanceArrays {
    NSInteger maxHeapCount = [_maxHeap count];
    NSInteger minHeapCount = [_minHeap count];
    if (abs((int)(maxHeapCount - minHeapCount)) > 1) {
        if (maxHeapCount > minHeapCount) {
            [_minHeap addObject:[_maxHeap firstObject]];
            [_maxHeap removeObjectAtIndex:0];
        } else {
            [_maxHeap addObject:[_minHeap firstObject]];
            [_minHeap removeObjectAtIndex:0];
        }
        [_maxHeap balanceArrayWithAscendingOrder:NO];
        [_minHeap balanceArrayWithAscendingOrder:YES];
    }
}

- (NSNumber *)median {
    
    NSInteger maxHeapCount = [_maxHeap count];
    NSInteger minHeapCount = [_minHeap count];
    NSUInteger maxHeaphead = [[_maxHeap firstObject] unsignedIntegerValue];
    NSUInteger minHeapHead = [[_minHeap firstObject] unsignedIntegerValue];
    if ((maxHeapCount + minHeapCount) % 2 == 0) {
        double result = (maxHeaphead + minHeapHead)/2.0;
        return [NSNumber numberWithDouble:result];
    } else {
        return (maxHeapCount > minHeapCount)?
        [NSNumber numberWithInteger:maxHeaphead]:
        [NSNumber numberWithInteger:minHeapHead];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
