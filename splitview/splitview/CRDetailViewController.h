//
//  CRDetailViewController.h
//  splitview
//
//  Created by Crisredfi on 04/08/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CRDetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
