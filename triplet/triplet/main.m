//
//  main.m
//  triplet
//
//  Created by Aleix Segon on 16/07/2014.
//  Copyright (c) 2014 mastercard. All rights reserved.
//

#import <Foundation/Foundation.h>


NSArray * getTriplet(NSString * inputString) {
    
    NSMutableArray *characters = [NSMutableArray array];
    [inputString enumerateSubstringsInRange:NSMakeRange(0, inputString.length)
                                options:NSStringEnumerationByComposedCharacterSequences
                                 usingBlock:^(NSString *substring,
                                              NSRange substringRange,
                                              NSRange enclosingRange,
                                              BOOL *stop) {
                                     [characters addObject:substring];
                                 }];
    

    NSMutableSet *numbers = [NSMutableSet setWithCapacity:3];
    while (numbers.count < 3) {
        NSInteger random = arc4random() % inputString.length;
        [numbers addObject:@(random)];
    }
    
    NSArray *orderedRandom = [[numbers allObjects] sortedArrayUsingSelector:@selector(compare:)];
    NSMutableArray *returnArray = [NSMutableArray array];
    for (int i = 0; i < 3; i++) {

        [returnArray addObject:[characters objectAtIndex:[orderedRandom[i] integerValue]]];
    }
    return returnArray;
    
    
    
}


int main(int argc, const char * argv[])
{

    @autoreleasepool {
// randomize array
        NSMutableArray *randomArray = [NSMutableArray arrayWithObjects:@2, @3, @5, @9, @12, @1, @7, nil];
        
        if ([randomArray count] > 0) {
        
            for (NSUInteger i = [randomArray count] - 1; i > 0; i--) {
                
                NSUInteger random = arc4random_uniform((int32_t)([randomArray count] -1));
                [randomArray exchangeObjectAtIndex:i withObjectAtIndex:random];
            }
        
            
        }
        NSLog(@"random array %@", randomArray);
        
        
        // insert code here...
        NSCountedSet *countedSet = [NSCountedSet set];
        for (int i = 0; i < 20; i++) {
            [countedSet addObjectsFromArray:getTriplet(@"helloworld")];
        }
        NSLog(@"%@", countedSet);

        NSLog(@"Hello, World!");
        
        
        
    }
    return 0;
}

