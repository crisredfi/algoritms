//
//  NSMutableArray+Heap.m
//  NSArrayCategories
//
//  Created by crisredfi on 24/09/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import "NSMutableArray+Heap.h"

@implementation NSMutableArray (Heap)

- (void)balanceExchangeFromIndex:(NSUInteger)origin toIndex:(NSUInteger)destination inAscendingOrder:(BOOL)isAscending {
    [self exchangeObjectAtIndex:origin withObjectAtIndex:destination];
    [self balanceObjectAtIndex:destination inAscendingOrder:isAscending];
    
}

- (void)balanceObjectAtIndex:(NSUInteger)childIndex inAscendingOrder:(BOOL)isAscending {
    NSUInteger parent = 0;
    if (childIndex > 1) {
        parent = childIndex/2;
        
        if (!isAscending) {
            if ([self objectAtIndex:childIndex -1] < [self objectAtIndex:parent -1]) {
                [self balanceExchangeFromIndex:childIndex-1 toIndex:parent - 1 inAscendingOrder:isAscending];
            }
        } else {
            if ([self objectAtIndex:childIndex -1] > [self objectAtIndex:parent -1]) {
                [self balanceExchangeFromIndex:childIndex-1 toIndex:parent - 1 inAscendingOrder:isAscending];
            }
        }
    }
}

- (void)balanceArrayWithAscendingOrder:(BOOL)isAscending {
    
    
    [self enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [self balanceObjectAtIndex:idx+1 inAscendingOrder:isAscending];
    }];
    
}

@end
