//
//  ViewController.m
//  LongStringParty
//
//  Created by crisredfi on 26/09/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString *string = @"johndoe_sdfjkldfj_ldfjlkfdsjfds_laleix_dfdjklfdsjs.txt";
    NSString *textWithoutExtension = [string stringByDeletingPathExtension];
    
    NSArray *words = [textWithoutExtension componentsSeparatedByCharactersInSet:[NSCharacterSet punctuationCharacterSet]];

    NSMutableDictionary *output = [NSMutableDictionary dictionary];
    NSMutableDictionary *seccondIndex = [NSMutableDictionary dictionary];
    [words enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        if (idx == 0) {
            output[@"NAME"] = [@[obj] mutableCopy];
        } else {
            NSString *key = [obj substringToIndex:1];
            NSString *remaining = [obj substringFromIndex:1];
            output[[NSString stringWithFormat:@"%ld",idx]] = remaining;
            seccondIndex[[NSString stringWithFormat:@"%ld",idx]] = key;

        }
    }];
    
    [output enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
       
        NSLog(@"%@ %@", seccondIndex[key], obj);
    }];
    CGPoint point = CGPointMake(10, 10);
    CGPoint origin = CGPointZero;
    
    CGFloat xDistance = point.x - origin.x;
    CGFloat yDistance = point.y - origin.y;
    
    CGFloat distance = sqrt(pow(xDistance, 2) + pow(yDistance, 2));
    NSLog(@"distance %f", distance);
 
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
