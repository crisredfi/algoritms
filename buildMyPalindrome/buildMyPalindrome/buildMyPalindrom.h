//
//  buildMyPalindrom.h
//  buildMyPalindrome
//
//  Created by aleix on 14/08/2014.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface buildMyPalindrom : NSObject

- (NSString *)buildPalindromeFromString:(NSString *)inputString;

@end
