//
//  WordsGenerator.m
//  TrieStructure
//
//  Created by Crisredfi on 13/09/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import "WordsGenerator.h"
#import "CRTrieStructure.h"

typedef void(^searchBlock)(NSString *word);


@implementation WordsGenerator


- (CRTrieStructure*)hasObject:(NSString *)object inArray:(NSArray *)container {
    
    __block BOOL hasObject = NO;
    __block CRTrieStructure *trie;
    [container enumerateObjectsUsingBlock:^(CRTrieStructure *obj, NSUInteger idx, BOOL *stop) {
        if ([obj.value isEqualToString:object]) {
            hasObject = YES;
            *stop = YES;
            trie = obj;
        }
    }];
    return  (hasObject != NO) ? trie : nil;
}

NSMutableSet *visitedDeepNodes;

-(void)deepFirstSearch:(CRTrieStructure *)searchNode
           withPattern:(NSArray *)pattern
              forWords:(NSString *)words
     completitionBlock:(searchBlock)block {
    
    if (!visitedDeepNodes) {
        // first time visiting the recursive function. lazy load the container
        // and add the first node to it
        visitedDeepNodes = [NSMutableSet set];
        [visitedDeepNodes addObject:searchNode];
    }
    
    for (CRTrieStructure *newNode in searchNode.childs) {
        if (![visitedDeepNodes containsObject:newNode]) {
            [visitedDeepNodes addObject:newNode];
            if ([newNode.value isEqualToString:[pattern firstObject]] ||
                [[pattern firstObject] isEqualToString:@"."]) {
                NSMutableArray *newPattern = [pattern mutableCopy];
                [newPattern removeObjectAtIndex:0];
               // [words enumerateObjectsUsingBlock:^(NSString *word, BOOL *stop) {
                    NSString *newWord = [words stringByAppendingString:newNode.value];
              //  }];
                if ([newNode.childs count] > 0 ) {
                   // deepFirstSearch(newNode,newPattern,newWords);
                    [self deepFirstSearch:newNode
                              withPattern:newPattern
                                 forWords:newWord
                        completitionBlock:block];
                } else {
                    block(newWord);
                    // return block... with the value we need.
                    
                }
            }
        }
    }
    
}


- (void)searchChilds:(NSArray *)container fromPattern:(NSString *)pattern {
    NSMutableArray *characters = [NSMutableArray array];
    [pattern enumerateSubstringsInRange:NSMakeRange(0, pattern.length)
                                options:NSStringEnumerationByComposedCharacterSequences
                             usingBlock:^(NSString *substring,
                                          NSRange substringRange,
                                          NSRange enclosingRange,
                                          BOOL *stop) {
                                 [characters addObject:substring];
                             }];
    
    // now iterate over the container array to breath the subchilds. do not add them in case index
    // pattern is not equal to specific chold
    CRTrieStructure *object = [self hasObject:characters[0] inArray:container];
    NSString *startPattern = characters[0];
    [characters removeObjectAtIndex:0];
    NSMutableSet *words = [NSMutableSet set];
    [self deepFirstSearch:object
              withPattern:characters
                 forWords:startPattern
        completitionBlock:^(NSString *word) {
            [words addObject:word];
        }];
    NSLog(@"words are %@", words);
}


- (void)initializeWordGenerator {
    // insert code here...
    NSArray *words = @[@"cat", @"cut", @"cot", @"cit", @"bat", @"bet"];
    NSMutableArray *containerArray = [NSMutableArray array]; // main container
    
    [words enumerateObjectsUsingBlock:^(NSString *word, NSUInteger idx, BOOL *stop) {
        __block CRTrieStructure *object;
        __block NSMutableArray *trieContainer = containerArray; // check for level
        [word enumerateSubstringsInRange:NSMakeRange(0, word.length)
                                 options:NSStringEnumerationByComposedCharacterSequences
                              usingBlock:^(NSString *substring,
                                           NSRange substringRange,
                                           NSRange enclosingRange,
                                           BOOL *stop) {
                                  if ([trieContainer count] > 0 ) {
                                      
                                      object = [self hasObject:substring inArray:trieContainer];
                                      if (object) {
                                          // we have the object, set the trie container child to be
                                          // current trie and continue in case we have more words.
                                          trieContainer = object.childs;
                                      } else {
                                          CRTrieStructure *trie = [[CRTrieStructure alloc]
                                                                   initWithValue:substring];
                                          [trieContainer addObject:trie]; // add current substring to container.
                                          trieContainer = trie.childs;
                                      }
                                  } else {
                                      // initialize array.
                                      CRTrieStructure *trie = [[CRTrieStructure alloc]
                                                               initWithValue:substring];
                                      [trieContainer addObject:trie]; // add current substring to container.
                                      trieContainer = trie.childs;
                                      
                                  }
                                  
                              }];
        
    }];
    [self searchChilds:containerArray fromPattern:@"b.t"];
    [containerArray sortedArrayUsingSelector:@selector(localizedCompare:)];
}



@end
