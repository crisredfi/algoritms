//
//  main.m
//  binaryArrayJunctions
//
//  Created by Crisredfi on 06/08/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

NSInteger intSort(id num1, id num2, void *context)
{
    NSInteger v1 = [num1 integerValue];
    NSInteger v2 = [num2 integerValue];
    if (v1 < v2)
        return NSOrderedAscending;
    else if (v1 > v2)
        return NSOrderedDescending;
    else
        return NSOrderedSame;
}

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        NSArray* input = @[@[@2, @5, @10],
                           @[@25, @100, @105],
                           @[@7, @56, @42]];
        
        NSMutableArray *totalArray = [NSMutableArray arrayWithArray:input[0]];
        NSData *hint = [totalArray sortedArrayHint];
        
//        for (int i = 1; i < input.count; i++) {
//            [totalArray addObjectsFromArray:input[i]];
//            NSArray *temporalArray = [totalArray sortedArrayUsingFunction:intSort context:nil hint:hint];
//            totalArray = [temporalArray mutableCopy];
//            hint = [totalArray sortedArrayHint];
//        }
        for (int i = 1; i < input.count; i++) {
            NSArray *tempo = input[i];
            
            for (int j =0; j < tempo.count; j++) {
                NSUInteger findIndex = [totalArray indexOfObject:tempo[j]
                                                   inSortedRange:NSMakeRange(0, input.count)
                                                         options:NSBinarySearchingFirstEqual
                                                 usingComparator:^(id obj1, id obj2)
                                        {
                                            return [obj1 compare:obj2];
                                        }];
                [totalArray insertObject:tempo[j] atIndex:findIndex];
            }
        }
        NSLog(@"sorted %@", totalArray);
        // insert code here...
        NSLog(@"Hello, World!");
        
    }
    return 0;
}

