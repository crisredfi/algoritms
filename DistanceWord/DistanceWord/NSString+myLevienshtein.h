//
//  NSString+myLevienshtein.h
//  DistanceWord
//
//  Created by Crisredfi on 13/07/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (myLevienshtein)

- (long)compareString:(NSString *)comparableString;

@end
