//
//  main.m
//  composedWords
//
//  Created by crisredfi on 29/10/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
        
        
        NSString *word = @"  hello World  ";
        NSArray *list = @[@"hello", @"World", @"face", @"book", @" "];
        NSMutableString *mutableWord = [word mutableCopy];
        word = [mutableWord stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        [list enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
            if ([mutableWord containsString:obj]) {
                NSRange range = [mutableWord rangeOfString:obj];
                [mutableWord deleteCharactersInRange:range];
                if ([mutableWord isEqualToString:@""]) {
                    NSLog(@"true");
                    *stop = YES;
                }
            }
        }];
        
        NSString *string = @"Lorem    ipsum dolar   sit  amet.";
        string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        NSArray *components = [string componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        components = [components filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self <> ''"]];
        
        string = [components componentsJoinedByString:@" "];
        NSLog(@"%@", string);
            }
    return 0;
}

NSArray *mergedAndSortedArrayForArrays(NSArray* arrays) {
    NSMutableSet* elements = [NSMutableSet set];
    for (NSArray* array in arrays) {
        assert([array isKindOfClass:[NSArray class]]);
        [elements addObjectsFromArray:array];
    }
    
    NSSortDescriptor* sortDescriptor = [NSSortDescriptor
                                        sortDescriptorWithKey:nil ascending:YES];
    return [elements sortedArrayUsingDescriptors:@[ sortDescriptor ]];
}
