//
//  Stack.m
//  Stack
//
//  Created by crisredfi on 29/10/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import "Stack.h"
#import "Node.h"

@interface Stack()

@property (strong, nonatomic) Node *node;

@end

@implementation Stack

- (instancetype)init {
    if (self = [super init]) {
        _node = nil;
    }
    return self;
}

- (void)push:(NSUInteger)item {
    Node *newnode = [Node new];
    newnode.value = item;

    if (self.node == nil) {
        self.node = newnode;
    } else {
        newnode.link = self.node;
        self.node = newnode;
    }
}

- (NSUInteger )pop {
    
    if (self.node == nil) {
        return 0;
    }
    
    Node *temporalNode = self.node;
    self.node = temporalNode.link;
    return  temporalNode.value;
    
}


@end
