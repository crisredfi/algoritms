//
//  main.m
//  DistanceWord
//
//  Created by Crisredfi on 13/07/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+Levenshtein.h"
#import "NSString+myLevienshtein.h"




int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        NSString *string = @"helo wrld";
        NSInteger distance = [string compareWithWord:@"ello world" matchGain:0 missingCost:1];
        long distanceLong = [string compareString:@"ello world"];
        // insert code here...
        NSLog(@"Hello, World! %lu", (long)distanceLong);
        
    }
    return 0;
}

