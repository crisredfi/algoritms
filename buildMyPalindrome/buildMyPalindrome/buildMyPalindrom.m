//
//  buildMyPalindrom.m
//  buildMyPalindrome
//
//  Created by aleix on 14/08/2014.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import "buildMyPalindrom.h"

@implementation buildMyPalindrom

- (NSString *)buildPalindromeFromString:(NSString *)inputString {
    
    // check length if its pair then we will have to have all items
    // duplicated. if its odds, one of the characters in the input string must be odd count.
    BOOL isPair = NO;
    if (inputString.length % 2 == 0) {
        // pairs
        isPair = YES;
    }
    NSMutableArray *characters = [NSMutableArray array];
    [inputString enumerateSubstringsInRange:NSMakeRange(0, inputString.length) options:NSStringEnumerationByComposedCharacterSequences usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
        
        [characters addObject:substring];
        
    }];

    

    
    
    NSCountedSet *countedSet = [NSCountedSet setWithArray:characters];
    // now we need to check if we need one odd or not, also we are now
    // verifying if all set has pair numnber of items.
    NSMutableArray *palindrome = [[NSMutableArray alloc] init];
    NSInteger index = 0;
    id oddObject;
    
    for (id obj in countedSet) {
        NSInteger count = [countedSet countForObject:obj];
        if (isPair && (count % 2 != 0)) {
            return @"NO PALINDROME";
        }
        if (!isPair && (count % 2 != 0)) {
            if (!oddObject) {
                oddObject = obj;
            } else {
                return @"NO PALINDROME, two odds...";

            }
        }
        
        if (obj != oddObject) {
            [palindrome insertObject:obj atIndex:index];
            [palindrome insertObject:obj atIndex:index+1];
            index++;
        }
        if (count > 2) {
            // randomly add at the beggining and at the end
            NSInteger i = 2;
            //if it has 3 items, and is the middle object. we will
    
            while ( i < count) {
                index++;
                [palindrome insertObject:obj atIndex:0];
                [palindrome insertObject:obj atIndex:[palindrome count]];
                i += 2;
            }
        }
    }

    if (!isPair && oddObject) {
        [palindrome insertObject:oddObject atIndex:index];
    }
    return  [palindrome componentsJoinedByString:@""];
    
}

@end
