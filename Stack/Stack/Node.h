//
//  Node.h
//  Stack
//
//  Created by crisredfi on 29/10/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Node : NSObject

@property (strong, nonatomic) Node *link;
@property (assign, nonatomic) NSUInteger value;

@end
