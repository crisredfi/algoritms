//
//  MedianHeap.m
//  heapDataStructure
//
//  Created by Crisredfi on 21/07/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import "MedianHeap.h"

@interface MedianHeap()

@property (nonatomic) NSMutableArray *lowHeap;
@property (nonatomic) NSMutableArray *highHeap;

@end

@implementation MedianHeap

- (instancetype)init
{

    self = [super init];
    if (self) {
        
        _lowHeap = [NSMutableArray arrayWithCapacity:10];
        _highHeap = [NSMutableArray arrayWithCapacity:10];
    }
    return self;
}


- (void)rebalanceLowHeap {
    // now rebalance low heap
    for (NSUInteger i = _lowHeap.count - 1; i >= 1;) {
        NSNumber *number = _lowHeap[i];
        NSNumber *parentNumber = _lowHeap[i/2];
        if ([parentNumber integerValue] > [number integerValue]) {
            // swap
            [_lowHeap exchangeObjectAtIndex:i withObjectAtIndex:i/2];
        }
        // handle tree structure pair/impair leaves.
        if (i%2!=0) {
            i--;
        } else {
            i = i/2;
        }
    }
}

- (void) addObject:(NSNumber *)newNumber
{
    [_lowHeap addObject:newNumber];
    if (_lowHeap.count > 2)
    {
        [self rebalanceLowHeap];
    }
    if (_lowHeap.count - _highHeap.count > 1) {
        [_highHeap insertObject:_lowHeap.firstObject atIndex:0];
        [_lowHeap removeObject:_lowHeap.firstObject];
    }
}


- (void)getMedian {
 // print them
    if ((_lowHeap.count + _highHeap.count) % 2 != 0) {
        NSLog(@"median is %lu" , [_lowHeap.firstObject integerValue]);
    } else {
        NSInteger summ = [_highHeap.firstObject integerValue] + [_lowHeap.firstObject integerValue];
        NSLog(@"median is %f" , (float)summ/2);

    }

    
    
}

@end
