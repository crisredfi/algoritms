//
//  subsets.m
//  allSubsets
//
//  Created by crisredfi on 13/10/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import "subsets.h"

@interface subsets ()

@property (nonatomic) NSMutableSet *mySubsets;

@end

@implementation subsets


- (instancetype)init
{
    self = [super init];
    if (self) {
        _mySubsets = [NSMutableSet set];
    }
    return self;
}




- (NSMutableArray *)getAllSubsetsOf:(NSInteger)number {
    
    if (number == 0) {
        NSMutableArray *newArray = [NSMutableArray array];
        [newArray addObject:[NSMutableArray array]];
        return newArray;
    }
    NSMutableArray *finalArray = [NSMutableArray array];

    for (NSInteger i = 1; i <= number; i++) {
        NSMutableArray *newArray = [self getAllSubsetsOf:number -i];
        
        [newArray enumerateObjectsUsingBlock:^(NSMutableArray *obj, NSUInteger idx, BOOL *stop) {
            [obj addObject:@(i)];
            [finalArray addObject:[obj mutableCopy]];
        }];
    }

    return finalArray;
}


- (void)printFinal {
    NSLog(@"finalArray %@", _mySubsets);
}

@end
