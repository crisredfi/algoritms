//
//  main.m
//  phoneNumbers
//
//  Created by Crisredfi on 05/07/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

void getWordFromNumbers(NSString *word,
                        NSMutableArray *numbers,
                        NSMutableArray *result,
                        NSDictionary *letters) {
    
    if ([numbers count] > 0) {
        
        if ([numbers.firstObject intValue] == 0 |
            [numbers.firstObject intValue] == 1) {
            // discard this numbers, and try the next letter if it exist.
            NSMutableArray *newArray = [numbers mutableCopy];
            [newArray removeObject:numbers.firstObject];
            getWordFromNumbers(word, newArray, result, letters);
        } else {
            int currentNumber = [numbers.firstObject intValue];
            NSString *selectedNumber = [NSString stringWithFormat:@"%i", currentNumber];
            
            for (int i = 0; i < [letters[selectedNumber]  count]; i++) {
                NSString *obj = letters[selectedNumber][i];
                word = [word stringByAppendingString:obj];
                NSMutableArray *newArray = [numbers mutableCopy];
                [newArray removeObjectAtIndex:0];
                getWordFromNumbers(word, newArray, result, letters);
            }
        }
    } else {
        [result addObject:word];
    }
}



int main(int argc, const char * argv[])
{
    
    @autoreleasepool {
        
        // insert code here...
        NSLog(@"Hello, World!");
        NSDictionary *letters = @{@"2" : @[@"a", @"b", @"c"],
                                  @"3" : @[@"d", @"e", @"f"],
                                  @"4" : @[@"g", @"h", @"i"],
                                  @"5" : @[@"j", @"k", @"l"],
                                  @"6" : @[@"m", @"n", @"o"],
                                  @"7" : @[@"p", @"q", @"r", @"s"],
                                  @"8" : @[@"t", @"u", @"v"],
                                  @"9" : @[@"w", @"x", @"y", @"z"]};
        NSMutableArray *answers = [NSMutableArray array];
        NSString *word = @"";
        NSMutableArray *numbers = [NSMutableArray arrayWithObjects:@6, @6, @1, @9, @2, @8, @6, @2, @3,  nil];
        getWordFromNumbers(word, numbers, answers,letters);
        NSLog(@"results %@ count %lu", answers, (unsigned long)[answers count]);
        
        
        
    }
    return 0;
}

