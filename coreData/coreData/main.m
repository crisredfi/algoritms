//
//  main.m
//  coreData
//
//  Created by aleix on 13/08/2014.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CCDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CCDAppDelegate class]));
    }
}
