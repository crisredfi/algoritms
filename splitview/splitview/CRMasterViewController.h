//
//  CRMasterViewController.h
//  splitview
//
//  Created by Crisredfi on 04/08/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CRDetailViewController;

@interface CRMasterViewController : UITableViewController

@property (strong, nonatomic) CRDetailViewController *detailViewController;

@end
