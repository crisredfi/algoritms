//
//  subsets.h
//  allSubsets
//
//  Created by crisredfi on 13/10/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface subsets : NSObject

- (NSMutableArray *)getAllSubsetsOf:(NSInteger)number;
- (void)printFinal;

@end
