//
//  Stack.h
//  Stack
//
//  Created by crisredfi on 29/10/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Node;

@interface Stack : NSObject

- (void)push:(NSUInteger )value;
- (NSUInteger)pop;


@end
