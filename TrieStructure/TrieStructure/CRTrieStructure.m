//
//  CRTrieStructure.m
//  CodeSnippets
//
//  Created by Crisredfi on 08/09/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import "CRTrieStructure.h"

@implementation CRTrieStructure

- (instancetype)initWithValue:(NSString *)value {
    self = [super init];
    if (self) {
        _value = value;
        _childs = [NSMutableArray array];
        
    }
    return self;
}


- (void)printTrie {
    // enumerate childs and print them.
    [_childs enumerateObjectsUsingBlock:^(CRTrieStructure *obj, NSUInteger idx, BOOL *stop) {
        [obj printTrie];
    }];
    
    
}

@end
