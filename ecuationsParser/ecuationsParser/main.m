//
//  main.m
//  ecuationsParser
//
//  Created by Crisredfi on 15/07/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

float equationResult(NSMutableArray* equation,  NSInteger value, NSInteger *xCounter) {
    
    NSString *currentOperation;
    value = 0;
    // check all elements.
    while (equation.count > 0) {
        NSString *operation = [equation firstObject];
        [equation removeObjectAtIndex:0];
        // check if next element, in case it exist is an X and then check if its a number
        // or a positive or negative simbol
        if ([operation isEqualToString:@"("]) {
            value += equationResult(equation, 0, xCounter);
        }
        if ([operation isEqualToString:@")"]) {
            return value;
        }
        if (equation.count > 0) {
            // we have already removed that object, so we are now in the 0 index. check it agai!
            if ([[equation objectAtIndex:0] isEqualToString:@"x"]) {
                // this an x factor. we need to handle this accordinngly
                // increase i so we dont step over here again.
                [equation removeObjectAtIndex:0];
                if ([operation isEqualToString:@"+"]) {
                    xCounter++;
                } else if ([operation isEqualToString:@"-"]) {
                    xCounter--;
                } else {
                    // reaching this point, we should have a current operation value
                    if ([currentOperation isEqualToString:@"+"]) {
                        xCounter += [operation integerValue];
                    } else if ([currentOperation isEqualToString:@"-"]) {
                        xCounter -= [operation integerValue];
                    }
                    currentOperation = nil;
                }
            }
        }
        if ([operation isEqualToString:@"+"] || [operation isEqualToString:@"-"]) {
            currentOperation = operation;
        } else if ([operation isEqualToString:@"x"]) {
            if (currentOperation != nil) {
                if ([currentOperation isEqualToString:@"+"]) {
                    xCounter += [operation integerValue];
                } else if ([currentOperation isEqualToString:@"-"]) {
                    xCounter -= [operation integerValue];
                }
                
            } else {
                xCounter++;
            }
            currentOperation = nil;
            
        } else if (currentOperation != nil) {
            
            if ([currentOperation isEqualToString:@"+"]) {
                value += [operation integerValue];
            } else if ([currentOperation isEqualToString:@"-"]) {
                value -= [operation integerValue];
                
            }
            currentOperation = nil;
        } else {
            value = [operation integerValue];
        }
    }
    // return the sum of the non x values. x values are stored through pointer
    return value;
    
    
}


int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        // insert code here...
        NSString *equation = @"x + ( 4 + 2 + x ) = - 3 - x";
        equation = [equation stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        // we now separate the string with one side and another,
        NSArray *separatedCoponent = [equation componentsSeparatedByString:@"="];
        // and now we are going to create an array of elements with the strings in the right side
        
        NSString *leftSide = separatedCoponent[1];

       NSArray *leftarray =  [[leftSide componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] mutableCopy];
        leftarray = [leftarray filteredArrayUsingPredicate:[NSPredicate
                                                            predicateWithFormat:@"self <> ''"]];
        NSMutableArray *leftSideElements = [leftarray mutableCopy];
        if (![leftSideElements[0] isEqualToString:@"-"]) {
            [leftSideElements insertObject:@"+" atIndex:0];

        }
        [leftSideElements enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
            if ([obj isEqualToString:@"-"]) {
                [leftSideElements removeObjectAtIndex:idx];
                [leftSideElements insertObject:@"+" atIndex:idx];
            } else if ([obj isEqualToString:@"+"]) {
                [leftSideElements removeObjectAtIndex:idx];
                [leftSideElements insertObject:@"-" atIndex:idx];
            }
            
        }];
        // finally put the elements in place
        NSArray *rightArray =  [[separatedCoponent[0] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] mutableCopy];
        rightArray = [rightArray filteredArrayUsingPredicate:[NSPredicate
                                                            predicateWithFormat:@"self <> ''"]];

        NSMutableArray *finalArray = [rightArray mutableCopy];
        
        [finalArray addObjectsFromArray:leftSideElements];
    
        // yey we have now the full string...
        NSInteger integer = 0;
        NSLog(@"%f", equationResult(finalArray, 0, integer));
        NSLog(@"%li", (long)integer);
        
        
        
        
        NSLog(@"Hello, World!");
        
    }
    return 0;
}

