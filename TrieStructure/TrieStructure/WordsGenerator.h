//
//  WordsGenerator.h
//  TrieStructure
//
//  Created by Crisredfi on 13/09/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WordsGenerator : NSObject

- (void)initializeWordGenerator;

@end
