//
//  ViewController.m
//  alphabetCombinations
//
//  Created by crisredfi on 15/09/14.
//  Copyright (c) 2014 Crisredfi. All rights reserved.
//

#import "ViewController.h"
#import "FindCombinations.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [FindCombinations getSoultionsForStringInput:@"1221"];    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
