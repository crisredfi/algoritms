//
//  main.m
//  heapDataStructure
//
//  Created by Crisredfi on 21/07/14.
//  Copyright (c) 2014 crisredfi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HeapStructure.h"
#import "MedianHeap.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        // insert code here...
        NSLog(@"Hello, World!");
        
        MedianHeap *heapStructure = [[MedianHeap alloc] init];
        [heapStructure addObject:@(2)];
        [heapStructure addObject:@(7)];
        [heapStructure addObject:@(4)];
        [heapStructure addObject:@(9)];
        [heapStructure addObject:@(1)];
        [heapStructure addObject:@(5)];
        [heapStructure addObject:@(8)];
        [heapStructure addObject:@(3)];
        [heapStructure addObject:@(6)];
        [heapStructure addObject:@(10)];
        [heapStructure addObject:@(11)];
        [heapStructure addObject:@(12)];
        [heapStructure addObject:@(13)];
        [heapStructure addObject:@(14)];
 
        [heapStructure getMedian];
        
    }
    return 0;
}

